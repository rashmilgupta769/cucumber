$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/java/features/cucumber.feature");
formatter.feature({
  "name": "Test cucumber",
  "description": "  I want to use this template for my feature file",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "test login",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Smoke"
    }
  ]
});
formatter.step({
  "name": "user is on \"login_page\"",
  "keyword": "Given "
});
formatter.step({
  "name": "user enters \"\u003cusername\u003e\" and \"\u003cpassword\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "enter details",
  "keyword": "And ",
  "rows": [
    {
      "cells": [
        "name",
        "lastname",
        "dob",
        "age"
      ]
    },
    {
      "cells": [
        "rashmil",
        "gupta",
        "13071993",
        "26"
      ]
    }
  ]
});
formatter.step({
  "name": "enter click",
  "keyword": "And "
});
formatter.step({
  "name": "home page should be displayed",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "username",
        "password"
      ]
    },
    {
      "cells": [
        "mayuri",
        "2222"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "testing \"smoke\" and \"daily\"",
  "keyword": "Given "
});
formatter.match({
  "location": "MySteps.testing_and(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "test login",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Smoke"
    }
  ]
});
formatter.step({
  "name": "user is on \"login_page\"",
  "keyword": "Given "
});
formatter.match({
  "location": "MySteps.user_is_on(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user enters \"mayuri\" and \"2222\"",
  "keyword": "When "
});
formatter.match({
  "location": "MySteps.user_enters_and(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enter details",
  "rows": [
    {
      "cells": [
        "name",
        "lastname",
        "dob",
        "age"
      ]
    },
    {
      "cells": [
        "rashmil",
        "gupta",
        "13071993",
        "26"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "MySteps.enter_details(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enter click",
  "keyword": "And "
});
formatter.match({
  "location": "MySteps.enter_click()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "home page should be displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "MySteps.home_page_should_be_displayed()"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "test login 2",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Daily"
    }
  ]
});
formatter.step({
  "name": "user is on \"login_page\"",
  "keyword": "Given "
});
formatter.step({
  "name": "user enters \"\u003cusername\u003e\" and \"\u003cpassword\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "enter details",
  "keyword": "And ",
  "rows": [
    {
      "cells": [
        "name",
        "lastname",
        "dob",
        "age"
      ]
    },
    {
      "cells": [
        "pushpil",
        "gupta",
        "15011992",
        "27"
      ]
    }
  ]
});
formatter.step({
  "name": "enter click",
  "keyword": "And "
});
formatter.step({
  "name": "home page should be displayed",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "username",
        "password"
      ]
    },
    {
      "cells": [
        "noori",
        "1111"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "testing \"smoke\" and \"daily\"",
  "keyword": "Given "
});
formatter.match({
  "location": "MySteps.testing_and(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "test login 2",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Daily"
    }
  ]
});
formatter.step({
  "name": "user is on \"login_page\"",
  "keyword": "Given "
});
formatter.match({
  "location": "MySteps.user_is_on(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user enters \"noori\" and \"1111\"",
  "keyword": "When "
});
formatter.match({
  "location": "MySteps.user_enters_and(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enter details",
  "rows": [
    {
      "cells": [
        "name",
        "lastname",
        "dob",
        "age"
      ]
    },
    {
      "cells": [
        "pushpil",
        "gupta",
        "15011992",
        "27"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "MySteps.enter_details(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enter click",
  "keyword": "And "
});
formatter.match({
  "location": "MySteps.enter_click()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "home page should be displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "MySteps.home_page_should_be_displayed()"
});
formatter.result({
  "status": "passed"
});
});