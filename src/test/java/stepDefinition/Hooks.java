package stepDefinition;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	
	@Before("@Regression")
	public void beforeSmoke() {
		System.out.println("Before Regression");
	}
	@After("@Regression")
	public void afterSmoke() {
		System.out.println("After Regression");
	}
}
