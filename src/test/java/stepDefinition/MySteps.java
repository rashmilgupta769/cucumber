
package stepDefinition;

import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.runner.RunWith;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When; 
import cucumber.api.junit.Cucumber;
import io.cucumber.datatable.DataTable;

@RunWith(Cucumber.class)
public class MySteps {
	@Given("user is on {string}")
	public void user_is_on(String string) {
	   System.out.println(string);
	}

	@Given("testing {string} and {string}")
	public void testing_and(String string, String string2) {
		System.out.print(string+"  "+string2);
	    System.out.println();
	}

@When("user enters {string} and {string}")
public void user_enters_and(String string, String string2) {
    System.out.print(string+"  "+string2);
    System.out.println();
}
	
	@When("enter details")
	public void enter_details(DataTable dataTable) {
		List<Map<String, String>> table=dataTable.asMaps();
		for(Map<String,String> map:table) {
			System.out.print(map.get("name")+" ");
			System.out.print(map.get("lastname")+" ");
			System.out.print(map.get("dob")+" ");
			System.out.print(map.get("age")+" ");
		}
		/*System.out.print(table.get(0).get(0)+" "+table.get(0).get(1)+" "+table.get(0).get(2)+" "+table.get(0).get(3)+" ");
		System.out.print(table.get(1).get(0)+" "+table.get(1).get(1)+" "+table.get(1).get(2)+" "+table.get(1).get(3)+" ");*/
	}
	

	@When("^enter click$")
	public void enter_click()  {
		System.out.println("click");
		 Assert.assertTrue(true);
	 
	}

	@Then("^home page should be displayed$")
	public void home_page_should_be_displayed()  {
		System.out.println("home");
		 Assert.assertTrue(true);
	   
	}
}
